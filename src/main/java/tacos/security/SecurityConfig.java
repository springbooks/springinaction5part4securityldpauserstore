package tacos.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.ldapAuthentication()
//                .userSearchFilter("(uid={0})")  // provide filters for the base LDAP queries
//                .groupSearchFilter("member={0}");  // by default this queries is empty indicating that search will be done from root of LDAP hierarchy
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication()
                .userSearchBase("ou=people")  //  method provides a base query for finding users.
                .userSearchFilter("(uid={0})")
                .groupSearchBase("ou=groups")
                .groupSearchFilter("member={0}")
                .passwordCompare()
                .passwordEncoder(new BCryptPasswordEncoder())
                .passwordAttribute("passcode")
                // by default spring security assumes that LDAP server is listening port 33389 on localhost
                // but if your LDAP server on different machine you can use contextSource() to configure location
//                .and()
//                .contextSource()
//                .url("ldap://tacocloud.com:389/dc=tacocloud,dc=com");

                // embedded LDAP server
                .and()
                .contextSource()
                // When the LDAP server starts, it will attempt to load data from any LDIF files that it
                // can find in the classpat
//        LDIF (LDAP Data Interchange Format)
                .root("dc=tacocloud,dc=com")
                .ldif("classpath:users.ldif")  // explicit way to tell what file to load
        ;

    }


}
