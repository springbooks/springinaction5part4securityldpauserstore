package tacos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Taco_Order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date placedAt;

    @JsonProperty("deliveryName")
    @Column(name = "delivery_name")
    @Size(max = 50, message = "must less than {max} characters")
    @NotBlank
    private String name;

    @JsonProperty("deliveryStreet")
    @Size(max = 50, message = "must less than {max} characters")
    @Column(name = "delivery_street")
    @NotBlank
    private String street;

    @JsonProperty("deliveryCity")
    @Size(max = 50, message = "must less than {max} characters")
    @Column(name = "delivery_city")
    @NotBlank
    private String deliveryCity;

    @JsonProperty("deliveryState")
    @Size(max = 2, message = "must less than {max} characters")
    @Column(name = "delivery_state")
    @NotBlank
    private String state;

    @JsonProperty("deliveryZip")
    @Column(name = "delivery_zip")
    @Size(max = 10, message = "must less than {max} characters")
    @NotBlank
    private String deliveryZip;

    @JsonProperty("ccNumber")
    @Digits(integer = 16, fraction = 0, message="Invalid cc number")
    @NotBlank(message = "Not a valid credit card number")
    private String ccNumber;

    @JsonProperty("ccExpiration")
    @Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$", message = "Must be formatted MM/YY")
    private String ccExpiration;

    @JsonProperty("ccCVV")
    @Column(name = "cc_cvv")
    @Digits(integer = 3, fraction = 0, message="Invalid CVV")
    private String ccCVV;

    @ManyToMany(targetEntity = Taco.class)
    private List<Taco> tacos = new ArrayList<>();

    public Order() {
    }

    @PrePersist
    void placedAt() {
        this.placedAt = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDeliveryZip() {
        return deliveryZip;
    }

    public void setDeliveryZip(String deliveryZip) {
        this.deliveryZip = deliveryZip;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCcExpiration() {
        return ccExpiration;
    }

    public void setCcExpiration(String ccExpiration) {
        this.ccExpiration = ccExpiration;
    }

    public String getCcCVV() {
        return ccCVV;
    }

    public void setCcCVV(String ccCVV) {
        this.ccCVV = ccCVV;
    }

    public Long getId() {
        return id;
    }

    public List<Taco> getTacos() {
        return tacos;
    }

    public void setTacos(List<Taco> tacos) {
        this.tacos = tacos;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(Date placedAt) {
        this.placedAt = placedAt;
    }

    @Override
    public String toString() {
        return "Order{" +
                "name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", city='" + deliveryCity + '\'' +
                ", state='" + state + '\'' +
                ", zip='" + deliveryZip + '\'' +
                ", ccNumber='" + ccNumber + '\'' +
                ", ccExpiration='" + ccExpiration + '\'' +
                ", ccCVV='" + ccCVV + '\'' +
                '}';
    }

    public void addDesign(Taco saved) {
        tacos.add(saved);
    }
}
