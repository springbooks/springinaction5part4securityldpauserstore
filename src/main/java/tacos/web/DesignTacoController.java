package tacos.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import tacos.Ingredient;
import tacos.Order;
import tacos.Taco;
import tacos.repositories.IngredientRepository;
import tacos.repositories.TacoRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * order attribute should be kept in the session and available across multiple requests
 * @SessionAttributes annotation provides that
 */
@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

    private static final Logger log = LoggerFactory.getLogger(DesignTacoController.class);
    private final IngredientRepository ingredientRepo;
    private final TacoRepository tacoRepo;

    @Autowired
    public DesignTacoController(IngredientRepository ingredientRepo, TacoRepository tacoRepo) {
        this.ingredientRepo = ingredientRepo;
        this.tacoRepo = tacoRepo;
    }

    @GetMapping
    public String showDesignForm(Model model) {
        addAllIngredientsToModel(model);
        return "design";
    }

    /**
     * This annotation ensures that object will be created in the model
     */
    @ModelAttribute(name = "order")
    public Order order() {
        return new Order();
    }

    @ModelAttribute(name = "taco")
    public Taco taco() {
        return new Taco();
    }

    @PostMapping
    public String processDesign(@Valid @ModelAttribute("taco") Taco design, Errors errors, @ModelAttribute("order") Order order, Model model) {
        if (errors.hasErrors()) {
            addAllIngredientsToModel(model);
            return "design";
        }

        Taco saved = tacoRepo.save(design);
        order.addDesign(saved);
        System.out.println(order.getTacos().size());
        return "redirect:/orders/current";
    }

    private List<Ingredient> filterByType(List<Ingredient> ingredients, Ingredient.Type type) {
        return ingredients.stream()
                .filter(ingredient -> ingredient.getType().equals(type))
                .collect(Collectors.toList());
    }

    private void addAllIngredientsToModel(Model model) {
        List<Ingredient> ingredients = new ArrayList<>();


        ingredientRepo.findAll().forEach(ingredients::add);

        for (Ingredient.Type type : Ingredient.Type.values()) {
            String typeName = type.toString().toLowerCase();
            model.addAttribute(typeName, filterByType(ingredients, type));
        }
    }

}
